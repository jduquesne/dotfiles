#   -------------------------------
#   1.  ENVIRONMENT CONFIGURATION
#   -------------------------------
 
#   Change Prompt
#   ------------------------------------------------------------
    export PS1="\u@\h \w \\$ \[$(tput sgr0)\]"
 
#   Set Paths
#   ------------------------------------------------------------
    export PATH="$PATH:/usr/local/bin/"
 
#   Set Default Editor
#   ------------------------------------------------------------
    export EDITOR=/usr/bin/vim
 
#   Set default blocksize for ls, df, du
#   ------------------------------------------------------------
    export BLOCKSIZE=1k
 
#   Add color to terminal
#   ------------------------------------------------------------
    export CLICOLOR=1
    export LSCOLORS=ExFxBxDxCxegedabagacad

#   Add auto-completion
#   ------------------------------------------------------------
    if [ -f $(brew --prefix)/etc/bash_completion ]; then
      . $(brew --prefix)/etc/bash_completion
    fi


#   -----------------------------
#   2.  MAKE TERMINAL BETTER
#   -----------------------------

alias ls='ls -Gp'                           # Preferred ‘ls’ implementation
alias la='la -Gp'                           # Preferred 'la' implementation
alias cp='cp -iv'                           # Preferred 'cp' implementation
alias mv='mv -iv'                           # Preferred 'mv' implementation
alias mkdir='mkdir -pv'                     # Preferred 'mkdir' implementation
alias ll='ls -FGlAhp'                       # Preferred 'ls' implementation

#   -----------------------------
#   3.  BASH HISTORY
#   -----------------------------

export HISTTIMEFORMAT="%h %d %H:%M:%S "     # Add Date and Time to Bash History
export HISTSIZE=10000                       # Expand the number of commands in history
export HISTFILESIZE=10000                   # Expand the number of lines in history
shopt -s histappend                         # Append to bash history instead of replacing
PROMPT_COMMAND='history -a'                 # Save bash history on the go
shopt -s cmdhist                            # Store multi-line commands in one history entry
